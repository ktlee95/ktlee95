package top5websiteranking.ktlee.com.top5websiteranking;

/**
 * Created by lenovo on 7/20/2017.
 */

//Configuration File for database link
public class Config {
    //the php files and database are uploaded online, use these link to download the data
    public static final String URL_GET_WEBSITE_BY_DATE = "http://ktlee.000webhostapp.com/top5website/getWebsiteByDate.php";
    public static final String URL_GET_DATE = "http://ktlee.000webhostapp.com/top5website/getAllDate.php";
    public static final String URL_GET_ALL = "http://ktlee.000webhostapp.com/top5website/getAllWebsite.php";

    //JSON Tags, which is the result get from the php
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "ID";
    public static final String TAG_DATE = "Date";
    public static final String TAG_WEBSITE = "Website";
    public static final String TAG_VISITS = "Visits";
}