package top5websiteranking.ktlee.com.top5websiteranking;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by lenovo on 7/20/2017.
 */

//class to store the properties of websites
public class Website{
    private int id;
    private String website;
    private String date;
    private Double visits;

    public Website(int id, String website, String date, Double visits) {
        this.id = id;
        this.website = website;
        this.date = date;
        this.visits = visits;
    }


    public int getId() {
        return id;
    }

    public String getWebsite() {
        return website;
    }

    public String getDate() {
        return date;
    }

    public Double getVisits() {
        return visits;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setVisits(Double visits) {
        this.visits = visits;
    }

}
