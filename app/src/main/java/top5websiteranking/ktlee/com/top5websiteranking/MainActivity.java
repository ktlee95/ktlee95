package top5websiteranking.ktlee.com.top5websiteranking;

import android.app.ProgressDialog;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static android.R.drawable.arrow_down_float;

public class MainActivity extends AppCompatActivity {
    //initialize all the variable
    Spinner typeSort;
    TextView dateSortTextView;
    Spinner dateSort;
    TableLayout table;
    List<String> dateSortList = new ArrayList<String>();
    //JSON_STRING is to store the database inside
    private String JSON_STRING;
    ArrayList<Website> websites = new ArrayList<Website>();
    int websitesCount = 0;
    TextView websiteTextView;
    TextView visitTextView;
    ImageView sortImageWebsite;
    ImageView sortImageVisits;

    private float[] yData;
    private String[] xData;
    PieChart pieChart;
    //sortType 0 to 4
    //0 = Total Visits in Descending Order
    //1 = Total Visits in Ascending Order
    //2 = Domain Name in Ascending Order
    //3 = Domain Name in Descending Order
    int sortType = 0;

    //link all the variable with xml objects, addListener, and have one round of extracting data from Database first
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        typeSort = (Spinner) findViewById(R.id.typeSort);
        dateSortTextView = (TextView) findViewById(R.id.dateSortTextView);
        dateSort = (Spinner) findViewById(R.id.dateSort);
        table = (TableLayout) findViewById(R.id.table);
        pieChart = (PieChart) findViewById(R.id.pieChart);
        websiteTextView = (TextView) findViewById(R.id.websiteTextView);
        visitTextView = (TextView) findViewById(R.id.visitTextView);
        sortImageWebsite = (ImageView) findViewById(R.id.sortImageWebsite);
        sortImageVisits = (ImageView) findViewById(R.id.sortImageVisits);

        websiteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sortType == 2)
                    sortType = 3;
                else
                    sortType = 2;
                sortByType();
            }
        });

        visitTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sortType == 0)
                    sortType = 1;
                else
                    sortType = 0;
                sortByType();
            }
        });

        List<String> typeSortList = new ArrayList<String>();
        typeSortList.add("Total Visits");
        typeSortList.add("Date");
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typeSortList);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSort.setAdapter(typeAdapter);

        typeSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position == 0) {
                    dateSortTextView.setVisibility(View.INVISIBLE);
                    dateSort.setVisibility(View.INVISIBLE);
                    sortType = 0;
                    getEntireDatabase();
                }
                else if(position == 1){
                    dateSortTextView.setVisibility(View.VISIBLE);
                    dateSort.setVisibility(View.VISIBLE);
                    sortType = 0;
                    getAllDate();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                typeSort.setSelection(0);
            }
        });

        dateSortList.clear();
        ArrayAdapter<String> dateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dateSortList);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateSort.setAdapter(dateAdapter);

        dateSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String dateSelected = dateSort.getSelectedItem().toString();
                StringBuilder sb = new StringBuilder();
                dateSelected = "?date="+ dateSelected;
                getWebsiteByDate(dateSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                dateSort.setSelection(0);
            }
        });

        getEntireDatabase();

    }

    //get the database record based on the selected Date
    private void getWebsiteByDate(String dateRequest) {
        class getWebsiteByDate extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;
            String dateRequest;
            public getWebsiteByDate( String dateRequest) {
                this.dateRequest = dateRequest;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this, "Requesting Data", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                //after get the database, store it into ArrayList
                storeAllWebsite();
                if(websites.size() == 0){
                    Toast.makeText(MainActivity.this, "No Website found", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                //sort the ArrayList and update the Table and TextView
                updateTableByVisitDesc();
                //recreate the pie chart
                createPieChart();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_WEBSITE_BY_DATE, dateRequest);
                return s;
            }
        }
        getWebsiteByDate gj = new getWebsiteByDate(dateRequest);
        gj.execute();
    }

    //store all the date that are extracted from the database
    private void storeAllDate() {
        JSONObject jsonObject = null;
        try {
            websitesCount = 0;
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Config.TAG_JSON_ARRAY);
            dateSortList.clear();
            for (int i = 0; i < result.length(); i++) {
                JSONObject jo = result.getJSONObject(i);
                String date = jo.getString(Config.TAG_DATE);
                dateSortList.add(date);
            }
            //sort the date ArrayList to get ascending date order
            Collections.sort(dateSortList, new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return lhs.compareTo(rhs);
                }
            });
            ArrayAdapter<String> dateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dateSortList);
            dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dateSort.setAdapter(dateAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //instruction to get all the date from the database
    private void getAllDate() {
        class getAllDate extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this, "Fetching Data", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                //store all the date into ArrayList
                storeAllDate();
                if(websites.size() == 0){
                    Toast.makeText(MainActivity.this, "Connection problem", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest(Config.URL_GET_DATE);
                return s;
            }
        }
        getAllDate gj = new getAllDate();
        gj.execute();
    }

    //determine what to sort based on user response(click)
    private void sortByType(){
        switch(sortType){
            case 0:
                //sort by popular website visit
                updateTableByVisitDesc();
                sortImageWebsite.setRotation(0);
                sortImageVisits.setRotation(0);
                break;
            case 1:
                //sort by unpopular website visit
                updateTableByVisitAsc();
                sortImageWebsite.setRotation(0);
                sortImageVisits.setRotation(180);
                break;
            case 2:
                //sort by domain name(ascending order)
                updateTableByNameAsc();
                sortImageVisits.setRotation(0);
                sortImageWebsite.setRotation(0);
                break;
            case 3:
                //sort by domain name(descending order)
                updateTableByNameDesc();
                sortImageVisits.setRotation(0);
                sortImageWebsite.setRotation(180);
                break;
            default:
                updateTableByVisitDesc();
                sortImageWebsite.setRotation(180);
                break;
        }
        //after sort, create the pie chart
        createPieChart();
    }

    //sort by domain name (Ascending order)
    private void updateTableByNameAsc(){
        //destroy all tablerow except the first row(header)
        while(table.getChildCount() > 1){
            table.removeViewAt(table.getChildCount() - 1);
        }
        //sort the ArrayList
        Collections.sort(websites, new Comparator<Website>() {
            @Override
            public int compare(Website lhs, Website rhs) {
                return lhs.getWebsite().compareTo(rhs.getWebsite());
            }
        });
        //create back the tablerow according to sort result
        for(int i=0; i < websites.size();i++){
            createNewTableRow(i);
        }
    }

    //sort by domain name (Descending order)
    private void updateTableByNameDesc(){
        //destroy all tablerow except the first row(header)
        while(table.getChildCount() > 1){
            table.removeViewAt(table.getChildCount() - 1);
        }
        //sort the ArrayList
        Collections.sort(websites, new Comparator<Website>() {
            @Override
            public int compare(Website lhs, Website rhs) {
                return rhs.getWebsite().compareTo(lhs.getWebsite());
            }
        });
        //create back the tablerow according to sort result
        for(int i=0; i < websites.size();i++){
            createNewTableRow(i);
        }
    }

    //sort by popular website
    private void updateTableByVisitDesc(){
        //destroy all tablerow except the first row(header)
        while(table.getChildCount() > 1){
            table.removeViewAt(table.getChildCount() - 1);
        }
        //sort the ArrayList
        Collections.sort(websites, new Comparator<Website>() {
            @Override
            public int compare(Website lhs, Website rhs) {
                return rhs.getVisits().compareTo(lhs.getVisits());
            }
        });
        //create back the tablerow according to sort result
        for(int i=0; i < websites.size();i++){
            createNewTableRow(i);
        }
    }

    //sort by unpopular website
    private void updateTableByVisitAsc(){
        //destroy all tablerow except the first row(header)
        while(table.getChildCount() > 1){
            table.removeViewAt(table.getChildCount() - 1);
        }
        //sort the ArrayList
        Collections.sort(websites, new Comparator<Website>() {
            @Override
            public int compare(Website lhs, Website rhs) {
                return lhs.getVisits().compareTo(rhs.getVisits());
            }
        });
        //create back the tablerow according to sort result
        for(int i=0; i < websites.size();i++){
            createNewTableRow(i);
        }
    }

    //create new row of table row and add a record into the table
    //websiteRowToAdd is which in the ArrayList to add
    private void createNewTableRow(int websiteRowToAdd){
        //set Layout
        TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
        TableRow.LayoutParams tableRowParams2 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
        TableRow tr = new TableRow(this);
        tr.setOrientation(TableLayout.VERTICAL);
        tr.setLayoutParams(new TableLayout.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        tr.setPadding(0, 0, 0, 0);

        //set textColor if the record is top 5 in the ArrayList based on sorted result
        int textColor;
        switch(websiteRowToAdd){
            case 0:
                textColor = Color.CYAN;
                break;
            case 1:
                textColor = Color.BLUE;
                break;
            case 2:
                textColor = Color.RED;
                break;
            case 3:
                textColor = Color.GREEN;
                break;
            case 4:
                textColor = Color.MAGENTA;
                break;
            default:
                textColor = Color.BLACK;
                break;
        }

        TextView website = new TextView(this);
        website.setTextColor(textColor);
        website.setGravity(Gravity.CENTER);
        website.setLayoutParams(tableRowParams);
        website.setText(websites.get(websiteRowToAdd).getWebsite());
        tr.addView(website);

        TextView visits = new TextView(this);
        visits.setPadding(0, 0, 20, 0);
        visits.setLayoutParams(tableRowParams2);
        visits.setGravity(Gravity.CENTER);
        visits.setTextColor(textColor);
        String visitsValue = Double.toString(websites.get(websiteRowToAdd).getVisits());
        NumberFormat nm = NumberFormat.getNumberInstance();
        visits.setText(nm.format(websites.get(websiteRowToAdd).getVisits()));
        tr.addView(visits);

        tr.setGravity(Gravity.TOP);
        table.addView(tr);
    }

    //extract all the data from the database
    private void getEntireDatabase() {
        class getEntireDatabase extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this, "Fetching Data", "Wait...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);
                loading.dismiss();
                JSON_STRING = s;
                //store the data into ArrayList
                storeAllWebsite();
                if(websites.size() == 0){
                    Toast.makeText(MainActivity.this, "Connection problem", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                //sort the ArrayList
                updateTableByVisitDesc();
                //create the pie chart according to sort result
                createPieChart();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequest(Config.URL_GET_ALL);
                return s;
            }
        }
        getEntireDatabase gj = new getEntireDatabase();
        gj.execute();
    }

    //create the pie chart
    private void createPieChart(){
        //to display only top 5 websites on pie chart
        int count = 5;
        float[] visitValue = new float[count];
        for(int i =0; i < count; i++){
            visitValue[i] = (float) websites.get(i).getVisits().floatValue();
            visitValue[i] = visitValue[i] / 1000000;
        }

        String[] websiteName = new String[count];
        for(int i =0; i < 5; i++){
            websiteName[i] = websites.get(i).getWebsite();
        }

        //turn the data to array
        yData = visitValue;
        xData = websiteName;
        //setup the descriptions for the pie chart and do some resize for the text
        Description description = pieChart.getDescription();
        description.setText("Total Visits(Millions)          ");
        pieChart.setDescription(description);
        pieChart.setRotationEnabled(true);
        pieChart.setHoleRadius(25f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterText("Websites");
        pieChart.setCenterTextSize(10);

        //add the data into the pie chart and draw the pie chart
        addDataSet();
        //add Listener when user click the pie chart
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Toast.makeText(MainActivity.this, "Website: " + websites.get((int)h.getX()).getWebsite() + "\n" + "Visits: " + String.format("%.1f", h.getY()) + "M",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    //add data into the pie chart and draw it
    private void addDataSet(){
        //turn data to entries
        ArrayList<PieEntry> yEntrys = new ArrayList<PieEntry>();
        ArrayList<String> xEntrys = new ArrayList<String>();

        for(int i = 0; i< xData.length; i++){
            xEntrys.add(xData[i]);
        }
        for(int i = 0; i< yData.length; i++){
            yEntrys.add(new PieEntry(yData[i], xData[i]));

        }

        //create the pie dataset
        PieDataSet pieDataSet = new PieDataSet(yEntrys,"");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(14);
        pieDataSet.setValueLineColor(Color.BLACK);

        //add different colors
        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.CYAN);
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.MAGENTA);
        colors.add(Color.GRAY);
        pieDataSet.setColors(colors);

        //create Legend for the chart
        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setEnabled(true);
        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);

        //create pieData object
        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextSize(12f);
        pieData.setValueTextColor(Color.BLACK);
        pieChart.setData(pieData);
        pieChart.highlightValue(null);
        pieChart.setRotationAngle(-0.35f);
        pieChart.animateY(1500);

        pieChart.invalidate();
    }

    //store all the data that are extracted from database into an ArrayList
    private void storeAllWebsite() {
        JSONObject jsonObject = null;
        try {
            websitesCount = 0;
            jsonObject = new JSONObject(JSON_STRING);
            JSONArray result = jsonObject.getJSONArray(Config.TAG_JSON_ARRAY);
            websites.clear();
            for (int i = 0; i < result.length(); i++) {
                JSONObject jo = result.getJSONObject(i);
                String id = jo.getString(Config.TAG_ID);
                String date = jo.getString(Config.TAG_DATE);
                String website = jo.getString(Config.TAG_WEBSITE);
                String visits = jo.getString(Config.TAG_VISITS);
                websites.add(new Website(Integer.parseInt(id), website, date, Double.parseDouble(visits)));
                websitesCount++;
            }
            //Website[] newWebsites = new Website[websites.length];

            for(int i=0; i< websites.size();i++){
                for(int j=i+1; j< websites.size(); j++){
                    if(websites.get(i).getWebsite().equals(websites.get(j).getWebsite())){
                        websites.get(i).setVisits(websites.get(i).getVisits()+websites.get(j).getVisits());
                        websites.remove(j);
                        j--;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
